package com.company.employees;

import com.company.abstracts.Employee;

import java.util.List;

public class Developers extends Employee<Developers>{

    public Developers(String name, int wage, String position) {
        super(name, wage, position);
    }

    @Override
    public List<Developers> getCoworkers() {
        return this.coworkers;
    }

    @Override
    public void work() {
        System.out.println("coding...");
    }




}
