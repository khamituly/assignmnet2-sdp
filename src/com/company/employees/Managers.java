package com.company.employees;

import com.company.abstracts.Employee;

import java.util.List;

public class Managers extends Employee<Managers> {

    public Managers(String name, int wage, String position) {
        super(name, wage, position);
    }

    @Override
    public List<Managers> getCoworkers() {
        return this.coworkers;
    }

    @Override
    public void work() {
        System.out.println("managing...");
    }
}
