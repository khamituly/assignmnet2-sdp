package com.company.employees;

import com.company.abstracts.Employee;

import java.util.List;

public class HR extends Employee<HR>{

    public HR(String name, int wage, String position) {
        super(name, wage, position);
    }

    @Override
    public List<HR> getCoworkers() {
        return this.coworkers;
    }



    @Override
    public void work() {
        System.out.println("search a employees...");
    }
}
