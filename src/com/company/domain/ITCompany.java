package com.company.domain;

import com.company.depratmens.HrDepartment;
import com.company.depratmens.ItDepartment;
import com.company.depratmens.ManagementDepartment;

    public class ITCompany {
        private HrDepartment hr;
        private ItDepartment it;
        private ManagementDepartment managers;

        public ITCompany(){

        }

        public void addHr(HrDepartment hr){
            this.hr = hr;
        }

        public void addIt(ItDepartment it){
            this.it = it;
        }

        public void addManagers(ManagementDepartment mg){
            this.managers = mg;
        }

        public HrDepartment getHr() {
            return hr;
        }

        public ItDepartment getIt() {
            return it;
        }

        public ManagementDepartment getManagers() {
            return managers;
        }
    }
