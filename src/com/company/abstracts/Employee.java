package com.company.abstracts;

import com.company.employees.Developers;

import java.util.ArrayList;
import java.util.List;

public abstract class Employee<T>{
    private String name;
    private int wage;
    private String position;
    protected List<T> coworkers;

    public Employee(String name,int wage, String position){
        this.name = name;
        this.wage =wage;
        this.position = position;
        this.coworkers = new ArrayList<>();
    }

    public void addCoworker(T coworker) {
        this.coworkers.add( coworker);
    }

    public String getName() {
        return name;
    }

    public int getWage() {
        return wage;
    }

    public String getPosition() {
        return position;
    }

    public void removeCoworker(Employee emp){
        this.coworkers.remove(emp);
    }

    public abstract List<T> getCoworkers();

    public abstract void work();

    @Override
    public String toString() {
        return "Name:"+name+", position:"+position+", wage:"+wage+"$";
    }
}
