package com.company;

import com.company.abstracts.Employee;
import com.company.depratmens.HrDepartment;
import com.company.depratmens.ItDepartment;
import com.company.depratmens.ManagementDepartment;
import com.company.domain.ITCompany;
import com.company.employees.Developers;
import com.company.employees.HR;
import com.company.employees.Managers;

public class Main {

    public static void main(String[] args) {
        ITCompany Google = new ITCompany();

        Employee employee1 = new Developers("Adam",1800,"senior BackEnd developer");
        Employee employee2 = new Developers("Pol",1300,"junior FrontEnd developer");
        Employee employee3 = new Developers("Billy",1400,"junior BackEnd developer");
        Employee employee4 = new HR("Lara",1100,"HR");
        Employee employee5 = new HR("Tom",1200,"HR");
        Employee employee6 = new Managers("Artur",1200,"manager");
        Employee employee7 = new Managers("Christopher",1300,"manager");

        employee1.addCoworker(employee2);
        employee1.addCoworker(employee3);
        employee2.addCoworker(employee1);
        employee2.addCoworker(employee3);

        employee5.addCoworker(employee4);
        employee4.addCoworker(employee5);

        employee7.addCoworker(employee6);
        employee6.addCoworker(employee7);

        ItDepartment it =new ItDepartment();
        it.addMember((Developers) employee1);
        it.addMember((Developers) employee2);
        it.addMember((Developers) employee3);

        HrDepartment hr = new HrDepartment();
        hr.addMember((HR) employee4);
        hr.addMember((HR) employee5);

        ManagementDepartment mg= new ManagementDepartment();
        mg.addMember((Managers) employee6);
        mg.addMember((Managers) employee7);

        Google.addIt(it);
        Google.addHr(hr);
        Google.addManagers(mg);


        for(HR hrEmp : Google.getHr().getMember(2).getCoworkers()){
            System.out.println(hrEmp);
        }
    }
}
