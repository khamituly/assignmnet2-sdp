package com.company.depratmens;

import com.company.employees.HR;

import java.util.ArrayList;
import java.util.List;

public class HrDepartment {
    private List<HR> hr;

    public HrDepartment(){
        this.hr = new ArrayList<>();
    }

    public void addMember(HR hr){
        this.hr.add(hr);
    }

    public HR getMember(int i){
        return hr.get(i);
    }

}
