package com.company.depratmens;

import com.company.employees.Developers;
import com.company.employees.Managers;

import java.util.ArrayList;
import java.util.List;

public class ManagementDepartment {
    private List<Managers> managers;

    public ManagementDepartment(){
        this.managers = new ArrayList<>();
    }

    public void addMember(Managers managers){
        this.managers.add(managers);
    }

    public Managers getMember(int i){
        return this.managers.get(i);
    }


}
