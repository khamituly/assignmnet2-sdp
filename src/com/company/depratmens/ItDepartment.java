package com.company.depratmens;

import com.company.employees.Developers;
import com.company.employees.HR;

import java.util.ArrayList;
import java.util.List;

public class ItDepartment {
    private List<Developers > developers;

    public ItDepartment(){
        this.developers = new ArrayList<>();
    }

    public void addMember(Developers developers){
        this.developers.add(developers);
    }

    public Developers getMember(int i){
        return developers.get(i);
    }

}
